﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SusquehannaTest
{
    class Program
    {
        public static int solution(int[] A)
        {
            if (A.Length < 5 || A.Length > 100000)
            {
                throw new ArgumentException("input array length should be between 5 and 100,000");
            }

            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] < 1 || A[i] > 1000000000)
                {
                    throw new ArgumentException("each array element should be between 1 and 1,000,000,000");
                }
            }

            int minCost = A[1] + A[A.Length - 2]; //assuming a minimum cost

            for (int i = 1; i < A.Length - 3; i++)
                for (int j = i + 2; j < A.Length - 1; j++)
                {
                    var currentCost = A[i] + A[j];
                    if (currentCost < minCost)
                        minCost = currentCost;
                }

            return minCost;
        }

        public class WordMachine
        {
            Stack<int> stack = new Stack<int>();

            private bool CheckElement(int value)
            {
                if (value < 0 || value > 1048575)  // Checking input range 0 .. 2^20 - 1
                {
                    throw new Exception($"Input number out of range : {value}");
                }

                return true;
            }

            public int Parse(string input)
            {
                try
                {
                    if (input.Length>2000)
                    {
                        throw new Exception("Input string should have a maximum length of 2000 characters!");
                    }

                    foreach (string rawInputCommand in input.Split(' '))
                    {
                        var inputCommand = rawInputCommand.Trim();

                        int parsedInt;

                        if (inputCommand == "DUP")
                        {
                            if (stack.Count < 1)
                            {
                                throw new Exception("Nothing to duplicate!");
                            }

                            stack.Push(stack.Peek());
                        }
                        else if (inputCommand == "POP")
                        {
                            if (stack.Count < 1)
                            {
                                throw new Exception("Nothing to pop out!");
                            }

                            stack.Pop();
                        }
                        else if (inputCommand == "+")
                        {
                            if (stack.Count < 2)
                            {
                                throw new Exception("Not enougth operands in the stack!");
                            }

                            var operand1 = stack.Pop();
                            var operand2 = stack.Pop();
                            var operationResult = operand1 + operand2;
                            CheckElement(operationResult);
                            stack.Push(operationResult);
                        }
                        else if (inputCommand == "-")
                        {
                            if (stack.Count < 2)
                            {
                                throw new Exception("Not enougth operands in the stack!");
                            }

                            var operand1 = stack.Pop();
                            var operand2 = stack.Pop();
                            var operationResult = operand1 - operand2;
                            CheckElement(operationResult);
                            stack.Push(operationResult);
                        }
                        else if (int.TryParse(inputCommand, out parsedInt))
                        {
                            CheckElement(parsedInt);
                            stack.Push(parsedInt);
                        }
                    }

                    if (stack.Count < 1)
                    {
                        throw new Exception("Stack is empty after all operations!");
                    }

                    return stack.Pop();

                }
                catch
                {
                    return -1;
                }

            }

        }

        public static int solution(string S)
        {
            var machine = new WordMachine();

            return machine.Parse(S);
        }


        static void Main(string[] args)
        {
            /*            var test1 = new int [6] {5,2,4,6,3,7};
                        var result = solution(test1);

                        var maxElementValue = 1000000000;
                        var test2 = new int[100000];
                        for (int i = 0; i < test2.Length; i++)
                        {
                            test2[i] = maxElementValue;
                        }
                        test2[1] = maxElementValue - 1;
                        test2[100000 - 2] = maxElementValue - 1;
                        result = solution(test2);
                        */

            //var result = solution("13 DUP 4 POP 5 DUP + DUP + -");
            //more than 2000 character input
            //var result = solution("                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ");
        }
    }
}
